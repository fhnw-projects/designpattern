package ch.fhnw.designpattern.presentationmodel;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class ColorEditorPM {

    private IntegerProperty rValue = new SimpleIntegerProperty();
    private IntegerProperty gValue = new SimpleIntegerProperty();
    private IntegerProperty bValue = new SimpleIntegerProperty();

    public IntegerProperty getRValueProperty() {
        return this.rValue;
    }

    public Integer getRValue() {
        return this.rValue.get();
    }

    public void setRValue(Integer v) {
        this.rValue.set(v);
    }

    public IntegerProperty getGValueProperty() {
        return this.gValue;
    }

    public Integer getGValue() {
        return this.gValue.get();
    }

    public void setGValue(Integer v) {
        this.gValue.set(v);
    }

    public IntegerProperty getBValueProperty() {
        return this.bValue;
    }

    public Integer getBValue() {
        return this.bValue.get();
    }

    public void setBValue(Integer v) {
        this.bValue.set(v);
    }

}
