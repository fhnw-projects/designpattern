package ch.fhnw.designpattern;

import ch.fhnw.designpattern.presentationmodel.ColorEditorPM;
import ch.fhnw.designpattern.view.ColorEditorUI;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class AppStarter extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent rootPanel = new ColorEditorUI(new ColorEditorPM());
        Scene scene = new Scene(rootPanel);
        scene.setFill(Color.BLACK);
        primaryStage.setScene(scene);
        primaryStage.setHeight(400);
        primaryStage.setWidth(400);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
