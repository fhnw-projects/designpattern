package ch.fhnw.designpattern.view;

import ch.fhnw.designpattern.presentationmodel.ColorEditorPM;
import ch.fhnw.designpattern.util.CustomNumberStringConverter;
import ch.fhnw.designpattern.util.NumberHexaStringConverter;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ColorEditorUI extends VBox {

    private ColorEditorPM colorEditorPM;
    private HBox hBox1, hBox2;
    private VBox sliderBox, textInputBox, labelBox, radioBox, buttonBox;
    private Slider rSlider, gSlider, bSlider;
    private TextField rText, gText, bText;
    private Label rLabel, gLabel, bLabel;
    private Rectangle rectangle;
    private Color color;
    private ToggleGroup radioGroup;
    private RadioButton red, blue, green, yellow, cyan, orange, black;
    private Button darker, brighter;

    public ColorEditorUI(ColorEditorPM colorEditorPM) {
        this.colorEditorPM = colorEditorPM;
        initControls();
        layoutControls();
        setupBindings();
        setupValueChangedListeners();
    }

    private void initControls() {

        this.hBox1 = new HBox();
        this.hBox2 = new HBox();
        this.sliderBox = new VBox();
        this.textInputBox = new VBox();
        this.labelBox = new VBox();
        this.radioBox = new VBox();
        this.buttonBox = new VBox();
        this.buttonBox = new VBox();

        this.rSlider = new Slider();
        this.gSlider = new Slider();
        this.bSlider = new Slider();
        this.rText = new TextField();
        this.gText = new TextField();
        this.bText = new TextField();
        this.rLabel = new Label();
        this.gLabel = new Label();
        this.bLabel = new Label();
        this.rectangle = new Rectangle(100, 100);
        this.color = Color.rgb(0, 0, 0);
        this.radioBox = new VBox();
        this.radioGroup = new ToggleGroup();
        this.red = new RadioButton("red");
        this.blue = new RadioButton("blue");
        this.green = new RadioButton("green");
        this.yellow = new RadioButton("yellow");
        this.cyan = new RadioButton("cyan");
        this.orange = new RadioButton("orange");
        this.black = new RadioButton("black");
        this.darker = new Button("Darker");
        this.brighter = new Button("Brighter");
    }

    private void layoutControls() {

        this.rSlider.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
        this.rSlider.setMax(255);
        this.rSlider.setMin(0);
        this.gSlider.setBackground(new Background(new BackgroundFill(Color.GREEN, null, null)));
        this.gSlider.setMax(255);
        this.gSlider.setMin(0);
        this.bSlider.setBackground(new Background(new BackgroundFill(Color.BLUE, null, null)));
        this.bSlider.setMax(255);
        this.bSlider.setMin(0);

        this.sliderBox.getChildren().addAll(this.rSlider, this.gSlider, this.bSlider);
        this.textInputBox.getChildren().addAll(this.rText, this.gText, this.bText);
        this.labelBox.getChildren().addAll(this.rLabel, this.gLabel, this.bLabel);

        this.hBox1.getChildren().addAll(this.sliderBox, this.textInputBox, this.labelBox);

        this.rectangle.setFill(this.color);
        this.radioGroup.getToggles().addAll(this.red, this.blue, this.green, this.yellow, this.cyan, this.orange,
                this.black);
        this.radioGroup.selectToggle(this.black);
        this.radioBox.getChildren().addAll(this.red, this.blue, this.green, this.yellow, this.cyan, this.orange,
                this.black);
        this.buttonBox.getChildren().addAll(this.darker, this.brighter);

        this.hBox2.getChildren().addAll(this.rectangle, this.radioBox, this.buttonBox);

        this.getChildren().setAll(this.hBox1, this.hBox2);
    }

    private void setupBindings() {

        NumberHexaStringConverter numberHexaStringConverter = new NumberHexaStringConverter();

        this.rText.textProperty().bindBidirectional(this.colorEditorPM.getRValueProperty(),
                new CustomNumberStringConverter(this.rText));
        this.rSlider.valueProperty().bindBidirectional(this.colorEditorPM.getRValueProperty());
        this.rLabel.textProperty().bindBidirectional(this.colorEditorPM.getRValueProperty(), numberHexaStringConverter);

        this.gText.textProperty().bindBidirectional(this.colorEditorPM.getGValueProperty(),
                new CustomNumberStringConverter(this.gText));
        this.gSlider.valueProperty().bindBidirectional(this.colorEditorPM.getGValueProperty());
        this.gLabel.textProperty().bindBidirectional(this.colorEditorPM.getGValueProperty(), numberHexaStringConverter);

        this.bText.textProperty().bindBidirectional(this.colorEditorPM.getBValueProperty(),
                new CustomNumberStringConverter(this.bText));
        this.bSlider.valueProperty().bindBidirectional(this.colorEditorPM.getBValueProperty());
        this.bLabel.textProperty().bindBidirectional(this.colorEditorPM.getBValueProperty(), numberHexaStringConverter);
    }

    private void setupValueChangedListeners() {

        this.colorEditorPM.getRValueProperty().addListener((ob, ov, nv) -> {
            this.rectangle.setFill(Color.rgb(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(),
                    this.colorEditorPM.getBValue()));
            setRadioButtonsColor(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(),
                    this.colorEditorPM.getBValue());
        });

        this.colorEditorPM.getGValueProperty().addListener((ob, ov, nv) -> {
            this.rectangle.setFill(Color.rgb(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(),
                    this.colorEditorPM.getBValue()));
            setRadioButtonsColor(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(),
                    this.colorEditorPM.getBValue());
        });

        this.colorEditorPM.getBValueProperty().addListener((ob, ov, nv) -> {
            this.rectangle.setFill(Color.rgb(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(),
                    this.colorEditorPM.getBValue()));
            setRadioButtonsColor(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(),
                    this.colorEditorPM.getBValue());
        });

        this.red.selectedProperty().addListener((ob, ov, nv) -> {
            setRgbColor(Color.RED);
        });

        this.blue.selectedProperty().addListener((ob, ov, nv) -> {
            setRgbColor(Color.BLUE);
        });

        this.green.selectedProperty().addListener((ob, ov, nv) -> {
            setRgbColor(Color.GREEN);
        });

        this.yellow.selectedProperty().addListener((ob, ov, nv) -> {
            setRgbColor(Color.YELLOW);
        });

        this.cyan.selectedProperty().addListener((ob, ov, nv) -> {
            setRgbColor(Color.CYAN);
        });

        this.orange.selectedProperty().addListener((ob, ov, nv) -> {
            setRgbColor(Color.ORANGE);
        });

        this.black.selectedProperty().addListener((ob, ov, nv) -> {
            setRgbColor(Color.BLACK);
        });

        this.darker.setOnAction(e -> {
            setRgbColor(Color
                    .rgb(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(), this.colorEditorPM.getBValue())
                    .darker());
        });

        this.brighter.setOnAction(e -> {
            setRgbColor(Color
                    .rgb(this.colorEditorPM.getRValue(), this.colorEditorPM.getGValue(), this.colorEditorPM.getBValue())
                    .brighter());
        });
    }

    private void setRadioButtonsColor(Integer r, Integer g, Integer b) {

        if (Color.RED.equals(Color.rgb(r, g, b))) {
            this.radioGroup.selectToggle(this.red);
        } else if (Color.GREEN.equals(Color.rgb(r, g, b))) {
            this.radioGroup.selectToggle(this.green);
        } else if (Color.BLUE.equals(Color.rgb(r, g, b))) {
            this.radioGroup.selectToggle(this.blue);
        } else if (Color.YELLOW.equals(Color.rgb(r, g, b))) {
            this.radioGroup.selectToggle(this.yellow);
        } else if (Color.CYAN.equals(Color.rgb(r, g, b))) {
            this.radioGroup.selectToggle(this.cyan);
        } else if (Color.ORANGE.equals(Color.rgb(r, g, b))) {
            this.radioGroup.selectToggle(this.orange);
        } else if (Color.BLACK.equals(Color.rgb(r, g, b))) {
            this.radioGroup.selectToggle(this.black);
        }
    }

    private void setRgbColor(Color rgb) {
        this.colorEditorPM.setRValue((int) (rgb.getRed() * 255));
        this.colorEditorPM.setGValue((int) (rgb.getGreen() * 255));
        this.colorEditorPM.setBValue((int) (rgb.getBlue() * 255));
    }

}
