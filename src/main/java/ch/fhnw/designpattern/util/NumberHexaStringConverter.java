package ch.fhnw.designpattern.util;

import javafx.util.StringConverter;

public class NumberHexaStringConverter extends StringConverter<Number> {

    @Override
    public String toString(Number number) {
        return Integer.toHexString(number.intValue());
    }

    @Override
    public Number fromString(String string) {
        return Integer.parseInt(string, 16);
    }

}
