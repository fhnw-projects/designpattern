package ch.fhnw.designpattern.util;

import javafx.scene.control.TextField;
import javafx.util.StringConverter;

public class CustomNumberStringConverter extends StringConverter<Number> {

    private TextField control;

    public CustomNumberStringConverter(TextField control) {
        this.setControl(control);
    }

    @Override
    public String toString(Number number) {
        if (number == null) {
            return "0";
        }

        if (number.intValue() > 255) {
            return "255";
        }

        if (number.intValue() < 0) {
            return "0";
        }

        return number.toString();
    }

    @Override
    public Number fromString(String string) {
        try {
            Integer nvInt = Integer.parseInt(string);
            if (nvInt > 255) {
                System.out.println("Invalid input, textfield set to: 255");
                this.control.setText("255");
                return 255;
            } else if (nvInt < 0) {
                System.out.println("Invalid input, textfield set to: 0");
                this.control.setText("0");
                return 0;
            }
            this.control.setStyle(null);
            return nvInt;

        } catch (NumberFormatException ex) {
            this.control.setStyle("-fx-text-box-border: red; -fx-focus-color: red;");
            System.out.println("Invalid input: " + string);
            this.control.setText("0");
            return 0;
        }
    }

    public TextField getControl() {
        return control;
    }

    public void setControl(TextField control) {
        this.control = control;
    }

}
